package homework6Test;

import org.homework6.*;

import java.util.Arrays;
import java.util.Objects;

import org.junit.Test;
import static org.junit.jupiter.api.Assertions.*;

public class FamilyTest {

    Family family = TestUtilities.setUpFamily();

    @Test
    public void addChildShouldAddChildToFamily() {
        Human child = new Human("Bob", "Doe", 1995);

        assertTrue(family.addChild(child));
        assertTrue(Arrays.asList(family.getChildren()).contains(child));

        // Check that the array is increased by one element
        assertEquals(1, family.getChildren().length);

        // Check that the added element is the specified child
        assertSame(child, family.getChildren()[0]);

        // Check that the family reference of the child is set correctly
        assertSame(family, child.getFamily());
    }

    @Test
    public void addChildShouldNotAddDuplicateChild() {
        Human child = new Human("Alex", "Doe", 2000);

        assertTrue(family.addChild(child));
        assertFalse(family.addChild(child));
    }

    @Test
    public void deleteChildShouldRemoveChildFromFamily() {
        Human child1 = new Human("Bob", "Doe", 1995);
        Human child2 = new Human("Alex", "Doe", 2000);

        assertTrue(family.addChild(child1));
        assertTrue(family.addChild(child2));

        // Check that the child is removed from the array
        family.deleteChild(child1);
        assertFalse(Arrays.asList(family.getChildren()).contains(child1));

        // Check that the array remains unchanged if a non-equivalent object is passed
        Human nonExistentChild = new Human("Non", "Existent", 1998);
        int originalChildrenCount = family.getChildren().length;

        family.deleteChild(nonExistentChild);

        assertEquals(originalChildrenCount, family.getChildren().length);
    }

    @Test
    public void deleteChildByIndexShouldRemoveChildFromFamily() {
        Human child1 = new Human("Bob", "Doe", 1995);
        Human child2 = new Human("Alex", "Doe", 2000);

        assertTrue(family.addChild(child1));
        assertTrue(family.addChild(child2));

        // Check that the child is removed from the array
        assertTrue(family.deleteChild(0)); // Assuming 0 is the index of child1
        assertFalse(Arrays.asList(family.getChildren()).contains(child1));

        // Check that the array remains unchanged if an out-of-range index is passed
        assertFalse(family.deleteChild(2)); // Assuming an out-of-range index

        // Check that the array remains unchanged and the method returns false
        assertEquals(1, family.getChildren().length);
    }

    @Test
    public void testCountFamily() {
        assertEquals(2, family.countFamily());

        Human child1 = new Human("Bob", "Doe", 1995);
        Human child2 = new Human("Alex", "Doe", 2000);

        assertTrue(family.addChild(child1));
        assertTrue(family.addChild(child2));

        assertEquals(4, family.countFamily());
        family.deleteChild(child1);
        assertEquals(3, family.countFamily());
    }

    @Test
    public void testSetFather_and_Mother() {
        Family family1 = new Family(null, null);
        Man father = new Man("John", "Doe", 1970);
        family1.setFather(father);

        assertEquals(father, family1.getFather());
        assertEquals(family1, father.getFamily());

        Woman mother = new Woman("Jane", "Doe", 1975);
        family1.setMother(mother);

        Man newFather = new Man("Bob", "Kennedy", 1970);
        family1.setFather(newFather);

        // Assert that the third parent is not set due to the error
        assertNull(newFather.getFamily());

        Human newMother = new Human("Marina", "Krotova", 1980);
        family1.setMother(newMother);

        // Assert that the third parent is not set due to the error
        assertNull(newMother.getFamily());
    }

    @Test
    public void testEquals_and_hashCode() {
        Man father2 = new Man("John", "Doe", 1970);
        Woman mother2 = new Woman("Jane", "Doe", 1975);
        Family family2 = new Family(father2, mother2);
        Man father3 = new Man("Charlie", "Brown", 1990);
        Woman mother3 = new Woman("Lucy", "Brown", 1992);
        Family family3 = new Family(father3, mother3);

        // Testing reflexivity
        assertEquals(family, family);

        // Testing consistency
        assertEquals(family, family2);

        // Testing symmetry
        assertEquals(family2, family);

        // Testing transitivity
        assertEquals(family, family2);
        assertEquals(family2, family);
        assertEquals(family, family2);

        // Testing equality with null
        assertNotEquals(family, null);

        // Testing hash code consistency
        assertEquals(family.hashCode(), family2.hashCode());

        // Testing hash code inequality with different objects
        assertNotEquals(family.hashCode(), family3.hashCode());
        assertEquals(family, family2);

        // Override hashCode for family1 to introduce a discrepancy
        family2 = new Family(father2, mother2) {
            @Override
            public int hashCode() {
                return Objects.hash(getFather(), getMother());
            }
        };

        assertNotEquals(family, family2);
        assertNotEquals(family.hashCode(), family2.hashCode());

        Man father4 = new Man("Charlie", "Brown", 1990);
        Woman mother4 = new Woman("Lucy", "Brown", 1992);
        Family family4 = new Family(father4, mother4);
        Human son_family3 = new Human("Bryan", "Brown", 2010);
        family4.addChild(son_family3);
        assertNotEquals(family3, family4);
    }

    @Test
    public void testSetChildren() {
        Human child1 = new Human("Bob", "Doe", 1995);
        Human child2 = new Human("Alex", "Doe", 2000);

        Human[] newChildren = {child1, child2};

        family.setChildren(newChildren);

        assertTrue(Arrays.asList(family.getChildren()).contains(child1));
        assertTrue(Arrays.asList(family.getChildren()).contains(child2));

        assertEquals(family, child1.getFamily());
        assertEquals(family, child2.getFamily());
    }

    @Test
    public void testToString() {
        Human child1 = new Human("Bob", "Doe", 2005);
        Human child2 = new Human("Alice", "Doe", 2010);
        family.addChild(child1);
        family.addChild(child2);

        Pet myPet = new Dog(); // Assuming Dog is a subclass of Pet
        myPet.setNickname("Buddy");
        family.setPet(myPet);

        // Act
        String familyString = family.toString();

        // Assert
        String expectedOutput = "Family{\n" +
                "  Father: " + family.getFather() + "\n" +
                "  Mother: " + family.getMother() + "\n" +
                "  Children: [" + child1 + ", " + child2 + "]\n" +
                "  Pet: " + myPet + "\n" +
                "  Total Persons in Family: 4\n" +
                "}";
        assertEquals(expectedOutput, familyString);
    }

    @Test
    public void testToStringWithNoChildrenAndNoPet() {
        String familyString = family.toString();

        // Assert
        String expectedOutput = "Family{\n" +
                "  Father: " + family.getFather() + "\n" +
                "  Mother: " + family.getMother() + "\n" +
                "  Children: No children in this family.\n" +
                "  Pet: null\n" +
                "  Total Persons in Family: 2\n" +
                "}";
        assertEquals(expectedOutput, familyString);
    }
}
