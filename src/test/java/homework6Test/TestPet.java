package homework6Test;

import org.homework6.Pet;
import org.homework6.Species;

public class TestPet extends Pet {
    public TestPet(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies(Species.UNKNOWN);
    }

    @Override
    public void respond() {
        // Implement abstract method
    }

    @Override
    public void eat() {
        // Implement abstract method
    }
}
