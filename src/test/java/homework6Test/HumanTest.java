package homework6Test;

import org.homework6.*;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Arrays;

import static homework6Test.TestNames.isInArray;
import static org.junit.jupiter.api.Assertions.*;

public class HumanTest {

    Family family = TestUtilities.setUpFamily();

    @Test
    public void testSetFamily() {
        Human human3 = new Human("Alex", "Doe", 2000);

        assertTrue(human3.setFamily(family));
        assertEquals(family, human3.getFamily());

        Family family2 = new Family(new Man("Bob", "Smith", 1990), new Woman("Alice", "Smith", 1995));

        assertFalse(human3.setFamily(family2));
        assertEquals(family, human3.getFamily()); // Family should remain unchanged
    }

    @Test
    public void testFeedPetWhenTimeToFeed() {
        Dog pet = new Dog(); // Assuming Dog is a subclass of Pet
        pet.setNickname("Buddy");
        pet.setTrickLevel(52);

        family.setPet(pet);

        // Test feeding when it's time to feed
        assertTrue(family.getFather().feedPet(true));
    }


    @Test
    public void testFeedPet_shouldReturnTrueOrFalse() {
        DomesticCat cat = new DomesticCat("Whiskers");
        // Assuming setTrickLevel generates a random value between 0 and 100
        cat.setTrickLevel(50);

        family.setPet(cat);

        // Now, instead of expecting a specific result, you can assert that it's either true or false
        assertTrue(Arrays.asList(true, false).contains(family.getFather().feedPet(false)));
    }

    @Test
    public void testFeedPet() {
        DomesticCat cat = new DomesticCat("Whiskers");
        family.setPet(cat);

        // Test when it's not time to feed, and pet trick level is higher
        family.getPet().setTrickLevel(100);
        assertTrue(family.getFather().feedPet(false));

        // Test when it's not time to feed, and pet trick level is small
        family.getPet().setTrickLevel(0);
        assertFalse(family.getFather().feedPet(false));
    }

    @Test
    public void testGreetPet() {
        Human child = new Human("Alex", "Doe", 2000);
        family.addChild(child);

        DomesticCat cat = new DomesticCat("Whiskers");
        family.setPet(cat);

        assertEquals("Hello, I greet you Whiskers", family.getMother().greetPet());

        assertEquals("Hello, my favorite friend Whiskers", family.getFather().greetPet());

        assertEquals("Hello, Whiskers", child.greetPet());

        // Test greeting when there is no pet
        family.setPet(null);
        assertEquals("I don't have a pet.", family.getMother().greetPet());
    }

    @Test
    public void testDescribePet() {
        Dog pet = new Dog(); // Assuming Dog is a subclass of Pet
        pet.setNickname("Buddy");
        pet.setAge(3);
        pet.setTrickLevel(52);
        family.setPet(pet);

        // Test describing the pet
        String describePet = family.getMother().describePet();
        System.out.println(describePet);
        assertEquals("I have a DOG. It is 3 years old, and it is very cunning.", describePet);

        // Test describing when there is no pet
        family.setPet(null);
        String describeNoPet = family.getMother().describePet();
        System.out.println(describeNoPet);
        assertEquals("I don't have a pet.", describeNoPet);
    }

    @Test
    public void testRepairCar() {
        Man man = new Man("John", "Doe", 1980);

        // Redirect System.out to capture the printed output
        ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStreamCaptor));

        // Call the respond() method
        man.repairCar();

        // Reset the standard output stream
        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));

        // Check only the output related to the respond() method
        String expectedOutput = "I'm repairing the car.";
        String actualOutput = outputStreamCaptor.toString().trim();  // Remove leading/trailing whitespaces

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testMakeUp() {
        Woman woman = new Woman("Alice", "Smith", 1980);

        // Redirect System.out to capture the printed output
        ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStreamCaptor));

        // Call the respond() method
        woman.makeup();

        // Reset the standard output stream
        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));

        // Check only the output related to the respond() method
        String expectedOutput = "I'm applying makeup.";
        String actualOutput = outputStreamCaptor.toString().trim();  // Remove leading/trailing whitespaces

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testBornChildWithNullName() {
        Human child = family.getMother().bornChild(null, "Doe", family, 115);

        family.getFather().setIQ(125);
        int fatherIQ = family.getFather().getIQ();

        family.getMother().setIQ(105);
        int motherIQ = family.getMother().getIQ();

        assertNotNull(child);
        assertTrue(child.getName() != null && !child.getName().isEmpty());
        assertEquals("Doe", child.getSurname());
        assertEquals(family, child.getFamily());
        int inheritedIQ = family.getMother().calculateInheritedIQ(fatherIQ, motherIQ);
        assertEquals((fatherIQ + motherIQ) / 2, inheritedIQ);
        assertTrue(child instanceof Man || child instanceof Woman);

        String randomMaleName = family.getMother().getRandomMaleName();
        assertNotNull(randomMaleName);
        assertTrue(isInArray(randomMaleName, family.getMother().MALE_NAMES));

        String randomFemaleName = family.getMother().getRandomFemaleName();
        assertNotNull(randomFemaleName);
        assertTrue(isInArray(randomFemaleName, family.getMother().FEMALE_NAMES));

        assertTrue(family.getMother().isMaleName("John"));
        assertFalse(family.getMother().isMaleName("Alice"));
    }

    @Test
    public void testEquals_and_hashCode() {
        Man human1 = new Man("John", "Doe", 1980);
        Man human2 = new Man("John", "Doe", 1980);
        Man human3 = new Man("Jane", "Doe", 1985);

        assertEquals(human1, human2);
        assertNotEquals(human1, human3);

        assertEquals(human1.hashCode(), human2.hashCode());
        assertNotEquals(human1.hashCode(), human3.hashCode());
    }

    @Test
    public void testToString() {
        Family family = new Family(new Man("Charlie", "Brown", 1990),
                new Woman("Lucy", "Brown", 1992));

        Human human = new Human("Bryan", "Brown", 2010, 120, family,
                DayOfWeek.TUESDAY.name(), "Studying", DayOfWeek.SATURDAY.name(), "Gym");

        assertEquals("Human{name='Bryan', surname='Brown', year=2010, iq=120, schedule=[[TUESDAY, Studying], [SATURDAY, Gym]]}",
                human.toString());

        Man man = new Man("John", "Doe", 1980);
        man.setIQ(120);

        // Test the string representation of the man
        assertEquals("Man{name='John', surname='Doe', year=1980, iq=120, schedule=[]}", man.toString());

        Woman woman = new Woman("Alice", "Smith", 1980);

        // Test the string representation of the woman
        assertEquals("Woman{name='Alice', surname='Smith', year=1980, iq=0, schedule=[]}", woman.toString());
    }
}
