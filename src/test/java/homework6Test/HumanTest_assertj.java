package homework6Test;

import org.homework6.*;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.PrintStream;

import static org.assertj.core.api.Assertions.assertThat;

public class HumanTest_assertj {

    Family family = TestUtilities.setUpFamily();

    @Test
    public void testSetFamily() {
        Human human3 = new Human("Alex", "Doe", 2000);

        assertThat(human3.setFamily(family)).isTrue();
        assertThat(human3.getFamily()).isEqualTo(family);

        Family family2 = new Family(new Man("Bob", "Smith", 1990), new Woman("Alice", "Smith", 1995));

        assertThat(human3.setFamily(family2)).isFalse();
        assertThat(human3.getFamily()).isEqualTo(family); // Family should remain unchanged
    }

    @Test
    public void testFeedPetWhenTimeToFeed() {
        Dog pet = new Dog(); // Assuming Dog is a subclass of Pet
        pet.setNickname("Buddy");
        pet.setTrickLevel(52);

        family.setPet(pet);

        // Test feeding when it's time to feed
        assertThat(family.getFather().feedPet(true)).isTrue();
    }

    @Test
    public void testFeedPet_shouldReturnTrueOrFalse() {
        DomesticCat cat = new DomesticCat("Whiskers");
        // Assuming setTrickLevel generates a random value between 0 and 100
        cat.setTrickLevel(50);

        family.setPet(cat);

        // Now, instead of expecting a specific result, you can assert that it's either true or false
        assertThat(family.getFather().feedPet(false)).isIn(true, false);
    }

    @Test
    public void testFeedPet() {
        DomesticCat cat = new DomesticCat("Whiskers");
        family.setPet(cat);

        // Test when it's not time to feed, and pet trick level is higher
        family.getPet().setTrickLevel(100);
        assertThat(family.getFather().feedPet(false)).isTrue();

        // Test when it's not time to feed, and pet trick level is small
        family.getPet().setTrickLevel(0);
        assertThat(family.getFather().feedPet(false)).isFalse();
    }

    @Test
    public void testGreetPet() {
        Human child = new Human("Alex", "Doe", 2000);
        family.addChild(child);

        DomesticCat cat = new DomesticCat("Whiskers");
        family.setPet(cat);

        assertThat(family.getMother().greetPet()).isEqualTo("Hello, I greet you Whiskers");

        assertThat(family.getFather().greetPet()).isEqualTo("Hello, my favorite friend Whiskers");

        assertThat(child.greetPet()).isEqualTo("Hello, Whiskers");

        // Test greeting when there is no pet
        family.setPet(null);
        assertThat(family.getMother().greetPet()).isEqualTo("I don't have a pet.");
    }

    @Test
    public void testDescribePet() {
        Dog pet = new Dog(); // Assuming Dog is a subclass of Pet
        pet.setNickname("Buddy");
        pet.setAge(3);
        pet.setTrickLevel(52);
        family.setPet(pet);

        // Test describing the pet
        String describePet = family.getMother().describePet();
        System.out.println(describePet);
        assertThat(describePet).isEqualTo("I have a DOG. It is 3 years old, and it is very cunning.");

        // Test describing when there is no pet
        family.setPet(null);
        String describeNoPet = family.getMother().describePet();
        System.out.println(describeNoPet);
        assertThat(describeNoPet).isEqualTo("I don't have a pet.");
    }

    @Test
    public void testRepairCar() {
        Man man = new Man("John", "Doe", 1980);

        // Redirect System.out to capture the printed output
        ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStreamCaptor));

        // Call the repairCar() method
        man.repairCar();

        // Reset the standard output stream
        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));

        // Check only the output related to the repairCar() method
        String expectedOutput = "I'm repairing the car.";
        String actualOutput = outputStreamCaptor.toString().trim();  // Remove leading/trailing whitespaces

        assertThat(actualOutput).isEqualTo(expectedOutput);
    }

    @Test
    public void testMakeUp() {
        Woman woman = new Woman("Alice", "Smith", 1980);

        // Redirect System.out to capture the printed output
        ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStreamCaptor));

        // Call the makeup() method
        woman.makeup();

        // Reset the standard output stream
        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));

        // Check only the output related to the makeup() method
        String expectedOutput = "I'm applying makeup.";
        String actualOutput = outputStreamCaptor.toString().trim();  // Remove leading/trailing whitespaces

        assertThat(actualOutput).isEqualTo(expectedOutput);
    }

    @Test
    void testBornChildWithNullName() {
        family.getMother().setIQ(105);
        family.getFather().setIQ(125);

        Human child = family.getMother().bornChild(null, "Doe", family, 115);

        assertThat(child).isNotNull();
        assertThat(child.getName()).isNotNull().isNotEmpty();
        assertThat(child.getSurname()).isEqualTo("Doe");
        assertThat(child.getFamily()).isEqualTo(family);

        int fatherIQ = family.getFather().getIQ();
        int motherIQ = family.getMother().getIQ();
        int inheritedIQ = family.getMother().calculateInheritedIQ(fatherIQ, motherIQ);

        assertThat(child.getIQ()).isEqualTo(inheritedIQ);
        assertThat(child).isInstanceOfAny(Man.class, Woman.class);

        String randomMaleName = family.getMother().getRandomMaleName();
        assertThat(randomMaleName).isNotNull();
        assertThat(family.getMother().MALE_NAMES).contains(randomMaleName);

        String randomFemaleName = family.getMother().getRandomFemaleName();
        assertThat(randomFemaleName).isNotNull();
        assertThat(family.getMother().FEMALE_NAMES).contains(randomFemaleName);

        assertThat(family.getMother().isMaleName("John")).isTrue();
        assertThat(family.getMother().isMaleName("Alice")).isFalse();
    }

    @Test
    public void testEquals_and_hashCode() {
        Human human1 = new Human("John", "Doe", 1980);
        Human human2 = new Human("John", "Doe", 1980);
        Human human3 = new Human("Jane", "Doe", 1985);

        assertThat(human1).isEqualTo(human2);
        assertThat(human1).isNotEqualTo(human3);

        assertThat(human1.hashCode()).isEqualTo(human2.hashCode());
        assertThat(human1.hashCode()).isNotEqualTo(human3.hashCode());
    }

    @Test
    public void testToString() {
        Family family = new Family(new Man("Charlie", "Brown", 1990),
                new Woman("Lucy", "Brown", 1992));

        Human human = new Human("Bryan", "Brown", 2010, 120, family,
                DayOfWeek.TUESDAY.name(), "Studying", DayOfWeek.SATURDAY.name(), "Gym");

        assertThat(human.toString())
                .isEqualTo("Human{name='Bryan', surname='Brown', year=2010, iq=120, schedule=[[TUESDAY, Studying], [SATURDAY, Gym]]}");

        Man man = new Man("John", "Doe", 1980);
        man.setIQ(120);

        // Test the string representation of the man
        assertThat(man.toString())
                .isEqualTo("Man{name='John', surname='Doe', year=1980, iq=120, schedule=[]}");

        Woman woman = new Woman("Alice", "Smith", 1980);

        // Test the string representation of the woman
        assertThat(woman.toString())
                .isEqualTo("Woman{name='Alice', surname='Smith', year=1980, iq=0, schedule=[]}");
    }
}
