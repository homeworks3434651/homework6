package homework6Test;

import org.homework6.Family;
import org.homework6.Man;
import org.homework6.Woman;

public class TestUtilities {

    public static Family setUpFamily() {
        Man father = new Man("John", "Doe", 1970);
        Woman mother = new Woman("Jane", "Doe", 1975);
        Family family = new Family(father, mother);
        return family;
    }
}
