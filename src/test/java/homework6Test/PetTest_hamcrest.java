package homework6Test;

import org.homework6.*;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Objects;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class PetTest_hamcrest {

    @Test
    public void testEat() {
        // Create a concrete subclass of Pet (e.g., Dog)
        Dog pet = new Dog();

        // Redirect the standard output stream
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call the eat() method
        pet.eat();

        // Reset the standard output stream
        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));

        // Check only the output related to the eat() method
        String expectedOutput = "I eat meat.";
        String actualOutput = outContent.toString().trim();  // Remove leading/trailing whitespaces

        assertThat(actualOutput, is(equalTo(expectedOutput)));
    }

    @Test
    public void testRespond() {
        // Create a concrete subclass of Pet (e.g., Dog)
        Dog pet = new Dog();
        pet.setNickname("TestPet");

        // Redirect the standard output stream
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call the respond() method
        pet.respond();

        // Reset the standard output stream
        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));

        // Check only the output related to the respond() method
        String expectedOutput = "Woof! I'm a dog.";
        String actualOutput = outContent.toString().trim();  // Remove leading/trailing whitespaces

        assertThat(actualOutput, is(equalTo(expectedOutput)));
    }

    @Test
    public void testFoul() {
        // Redirect the standard output stream
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Create a concrete subclass of Pet (e.g., Dog)
        Dog pet = new Dog();
        pet.foul();

        // Reset the standard output stream
        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));

        // Check only the output related to the foul() method
        String expectedOutput = "I left a little surprise for you on the carpet.";
        String actualOutput = outContent.toString().trim();  // Remove leading/trailing whitespaces

        assertThat(actualOutput, containsString(expectedOutput));
    }

    @Test
    public void testEquals_and_hashCode() {
        DomesticCat pet1 = new DomesticCat("Whiskers",3,  50, new String[]{"play", "sleep"});
        DomesticCat pet2 = new DomesticCat("Whiskers", 3, 50, new String[]{"play", "sleep"});
        DomesticCat pet3 = new DomesticCat("Thomas",2,  60, new String[]{"play", "rollover"});

        // Testing reflexivity
        assertThat(pet1, is(equalTo(pet1)));

        // Testing consistency
        assertThat(pet1, is(equalTo(pet2)));

        // Testing symmetry
        assertThat(pet2, is(equalTo(pet1)));

        // Testing transitivity
        assertThat(pet1, is(equalTo(pet2)));
        assertThat(pet2, is(equalTo(pet1)));
        assertThat(pet1, is(equalTo(pet2)));

        // Testing equality with null
        assertThat(pet1, is(not(equalTo(null))));

        // Testing hash code consistency
        assertThat(pet1.hashCode(), is(equalTo(pet2.hashCode())));

        // Testing hash code inequality with different objects
        assertThat(pet1.hashCode(), is(not(equalTo(pet3.hashCode()))));

        // Override hashCode for pet1 to introduce a discrepancy
        pet1 = new DomesticCat("Whiskers", 3, 50, new String[]{"play", "sleep"}) {
            @Override
            public int hashCode() {
                return Objects.hash(getAge());
            }
        };
        assertThat(pet1, is(equalTo(pet2)));
        assertThat(pet1.hashCode(), is(not(equalTo(pet2.hashCode()))));

        // Changing a field after object creation
        pet1.setAge(4);
        assertThat(pet1, is(not(equalTo(pet2))));
    }

    @Test
    public void testEqualsWhenSpeciesIsUnknown() {
        // Create two Pet objects with UNKNOWN species
        Pet pet1 = new TestPet("Buddy", 3, 50, new String[]{"play", "sleep"});
        Pet pet2 = new TestPet("Buddy", 3, 50, new String[]{"play", "sleep"});

        // Ensure the equals method works as expected
        assertThat(pet1, is(equalTo(pet2)));
        assertThat(pet2, is(equalTo(pet1)));
    }

    @Test
    public void toStringShouldReturnFormattedString() {
        Dog dog = new Dog("Buddy");

        String expected1 = String.format("Pet{%n" +
                        "    species=DOG%n" +
                        "    canFly=false%n" +
                        "    hasFur=true%n" +
                        "    numberOfLegs=4%n" +
                        "    nickname=Buddy%n" +   // Use %s for String values
                        "    age=0%n" +
                        "    trickLevel=0%n" +
                        "    habits=%s%n" +     // Use %s for String values
                        "}",
                (String) null);
        String actual1 = dog.toString();

        assertThat(actual1, is(equalTo(expected1)));

        Fish fish = new Fish("Nemo");

        String expected2 = String.format("Pet{%n" +
                        "    species=FISH%n" +
                        "    canFly=false%n" +
                        "    hasFur=false%n" +
                        "    numberOfLegs=0%n" +
                        "    nickname=Nemo%n" +   // Use %s for String values
                        "    age=0%n" +
                        "    trickLevel=0%n" +
                        "    habits=%s%n" +     // Use %s for String values
                        "}",
                (String) null);
        String actual2 = fish.toString();

        assertThat(actual2, is(equalTo(expected2)));
    }

    @Test
    public void testToStringWhenSpeciesIsUnknown() {
        // Create a Pet object with UNKNOWN species
        Pet pet = new TestPet("Buddy", 3, 50, new String[]{"play", "sleep"});

        // Define the expected output
        String expectedOutput = String.format("Pet{%n" +
                        "    species=%s%n" +
                        "    canFly=%s%n" +
                        "    hasFur=%s%n" +
                        "    numberOfLegs=%d%n" +
                        "    nickname=%s%n" +
                        "    age=%d%n" +
                        "    trickLevel=%d%n" +
                        "    habits=%s%n" +
                        "}",
                Species.UNKNOWN,
                Species.UNKNOWN.canFly(),
                Species.UNKNOWN.hasFur(),
                Species.UNKNOWN.getNumberOfLegs(),
                "Buddy",
                3,
                50,
                "[play, sleep]");

        // Ensure the toString method works as expected
        assertThat(pet.toString(), is(equalTo(expectedOutput)));
    }
}
