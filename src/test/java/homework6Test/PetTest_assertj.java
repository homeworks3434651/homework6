package homework6Test;

import org.homework6.Dog;
import org.homework6.DomesticCat;
import org.homework6.Pet;
import org.homework6.Species;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;

public class PetTest_assertj {

    @Test
    public void testEat() {
        // Create a concrete subclass of Pet (e.g., Dog)
        DomesticCat pet = new DomesticCat();

        // Redirect the standard output stream
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call the eat() method
        pet.eat();

        // Reset the standard output stream
        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));

        // Check only the output related to the eat() method
        String expectedOutput = "I eat cat food.";
        String actualOutput = outContent.toString().trim();  // Remove leading/trailing whitespaces

        assertThat(actualOutput).isEqualTo(expectedOutput);
    }

    @Test
    public void testRespond() {
        // Create a concrete subclass of Pet (e.g., Dog)
        Dog pet = new Dog();
        pet.setNickname("TestPet");

        // Redirect the standard output stream
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call the respond() method
        pet.respond();

        // Reset the standard output stream
        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));

        // Check only the output related to the respond() method
        String expectedOutput = "Woof! I'm a dog.";
        String actualOutput = outContent.toString().trim();  // Remove leading/trailing whitespaces

        assertThat(actualOutput).isEqualTo(expectedOutput);
    }

    @Test
    public void testFoul() {
        // Redirect the standard output stream
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Create a concrete subclass of Pet (e.g., Dog)
        Dog pet = new Dog();
        pet.foul();

        // Reset the standard output stream
        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));

        // Check only the output related to the foul() method
        String expectedOutput = "I left a little surprise for you on the carpet.";
        String actualOutput = outContent.toString().trim();  // Remove leading/trailing whitespaces

        assertThat(actualOutput).contains(expectedOutput);
    }

    @Test
    public void testEquals_and_hashCode() {
        DomesticCat pet1 = new DomesticCat("Whiskers",3,  50, new String[]{"play", "sleep"});
        DomesticCat pet2 = new DomesticCat("Whiskers", 3, 50, new String[]{"play", "sleep"});
        DomesticCat pet3 = new DomesticCat("Thomas", 2, 60, new String[]{"play", "rollover"});

        // Testing reflexivity
        assertThat(pet1).isEqualTo(pet1);

        // Testing consistency
        assertThat(pet1).isEqualTo(pet2);

        // Testing symmetry
        assertThat(pet2).isEqualTo(pet1);

        // Testing transitivity
        assertThat(pet1).isEqualTo(pet2).isEqualTo(pet1);

        // Testing equality with null
        assertThat(pet1).isNotEqualTo(null);

        // Testing hash code consistency
        assertThat(pet1.hashCode()).isEqualTo(pet2.hashCode());

        // Testing hash code inequality with different objects
        assertThat(pet1.hashCode()).isNotEqualTo(pet3.hashCode());

        // Override hashCode for pet1 to introduce a discrepancy
        pet1 = new DomesticCat("Whiskers", 3, 50, new String[]{"play", "sleep"}) {
            @Override
            public int hashCode() {
                return Objects.hash(getAge());
            }
        };
        assertThat(pet1).isEqualTo(pet2);
        assertThat(pet1.hashCode()).isNotEqualTo(pet2.hashCode());

        // Changing a field after object creation
        pet1.setAge(4);
        assertThat(pet1).isNotEqualTo(pet2);
    }

    @Test
    public void testEqualsWhenSpeciesIsUnknown() {
        // Create two Pet objects with UNKNOWN species
        Pet pet1 = new TestPet("Buddy", 3, 50, new String[]{"play", "sleep"});
        Pet pet2 = new TestPet("Buddy", 3, 50, new String[]{"play", "sleep"});

        // Ensure the equals method works as expected
        assertThat(pet1).isEqualTo(pet2);
        assertThat(pet2).isEqualTo(pet1);
    }

    @Test
    public void testToStringWhenSpeciesIsUnknown() {
        // Create a Pet object with UNKNOWN species
        Pet pet = new TestPet("Buddy", 3, 50, new String[]{"play", "sleep"});

        // Define the expected output
        String expectedOutput = String.format("Pet{%n" +
                        "    species=%s%n" +
                        "    canFly=%s%n" +
                        "    hasFur=%s%n" +
                        "    numberOfLegs=%d%n" +
                        "    nickname=%s%n" +
                        "    age=%d%n" +
                        "    trickLevel=%d%n" +
                        "    habits=%s%n" +
                        "}",
                Species.UNKNOWN,
                Species.UNKNOWN.canFly(),
                Species.UNKNOWN.hasFur(),
                Species.UNKNOWN.getNumberOfLegs(),
                "Buddy",
                3,
                50,
                "[play, sleep]");

        // Ensure the toString method works as expected
        assertThat(pet.toString()).isEqualTo(expectedOutput);
    }
}
