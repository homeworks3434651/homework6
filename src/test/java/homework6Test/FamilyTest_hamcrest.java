package homework6Test;

import org.hamcrest.Matchers;
import org.homework6.*;
import org.junit.Test;

import java.util.Arrays;
import java.util.Objects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class FamilyTest_hamcrest {

    Family family = TestUtilities.setUpFamily();

    @Test
    public void addChildShouldAddChildToFamily() {
        Human child = new Human("Bob", "Doe", 1995);

        assertThat(family.addChild(child), is(true));
        assertThat(Arrays.asList(family.getChildren()), hasItem(child));
        assertThat(family.getChildren().length, is(1));
        assertThat(family.getChildren()[0], sameInstance(child));
        assertThat(child.getFamily(), sameInstance(family));
    }

    @Test
    public void addChildShouldNotAddDuplicateChild() {
        Human child = new Human("Alex", "Doe", 2000);

        assertThat(family.addChild(child), is(true));
        assertThat(family.addChild(child), is(false));
    }

    @Test
    public void deleteChildShouldRemoveChildFromFamily() {
        Human child1 = new Human("Bob", "Doe", 1995);
        Human child2 = new Human("Alex", "Doe", 2000);

        assertThat(family.addChild(child1), is(true));
        assertThat(family.addChild(child2), is(true));

        family.deleteChild(child1);
        assertThat(Arrays.asList(family.getChildren()), not(hasItem(child1)));

        Human nonExistentChild = new Human("Non", "Existent", 1998);
        int originalChildrenCount = family.getChildren().length;

        family.deleteChild(nonExistentChild);
        assertThat(family.getChildren().length, is(originalChildrenCount));
    }

    @Test
    public void deleteChildByIndexShouldRemoveChildFromFamily() {
        Human child1 = new Human("Bob", "Doe", 1995);
        Human child2 = new Human("Alex", "Doe", 2000);

        assertThat(family.addChild(child1), is(true));
        assertThat(family.addChild(child2), is(true));

        assertThat(family.deleteChild(0), is(true));
        assertThat(Arrays.asList(family.getChildren()), not(hasItem(child1)));

        assertThat(family.deleteChild(2), is(false));
        assertThat(family.getChildren().length, is(1));
    }

    @Test
    public void testCountFamily() {
        assertThat(family.countFamily(), is(2));

        Human child1 = new Human("Bob", "Doe", 1995);
        Human child2 = new Human("Alex", "Doe", 2000);

        assertThat(family.addChild(child1), is(true));
        assertThat(family.addChild(child2), is(true));

        assertThat(family.countFamily(), is(4));

        family.deleteChild(child1);
        assertThat(family.countFamily(), is(3));
    }

    @Test
    public void testSetFather_and_Mother() {
        Family family = new Family(null, null);
        Man father = new Man("John", "Doe", 1970);
        family.setFather(father);

        assertThat(family.getFather(), is(father));
        assertThat(father.getFamily(), is(sameInstance(family)));

        Woman mother = new Woman("Jane", "Doe", 1975);
        family.setMother(mother);

        Man newFather = new Man("Bob", "Kennedy", 1970);
        family.setFather(newFather);

        assertThat(newFather.getFamily(), is(nullValue()));

        Woman newMother = new Woman("Marina", "Krotova", 1980);
        family.setMother(newMother);

        assertThat(newMother.getFamily(), is(nullValue()));
    }

    @Test
    public void testEquals_and_hashCode() {
        Man father2 = new Man("John", "Doe", 1970);
        Woman mother2 = new Woman("Jane", "Doe", 1975);
        Family family2 = new Family(father2, mother2);
        Man father3 = new Man("Charlie", "Brown", 1990);
        Woman mother3 = new Woman("Lucy", "Brown", 1992);
        Family family3 = new Family(father3, mother3);

        // Testing reflexivity
        assertThat(family, is(equalTo(family)));

        // Testing consistency
        assertThat(family, is(equalTo(family2)));

        // Testing symmetry
        assertThat(family2, is(equalTo(family)));

        // Testing transitivity
        assertThat(family, is(equalTo(family2)));
        assertThat(family2, is(equalTo(family)));
        assertThat(family, is(equalTo(family2)));

        // Testing equality with null
        assertThat(family, is(not(equalTo(null))));

        // Testing hash code consistency
        assertThat(family.hashCode(), is(equalTo(family2.hashCode())));

        // Testing hash code inequality with different objects
        assertThat(family.hashCode(), is(not(equalTo(family3.hashCode()))));
        assertThat(family, is(equalTo(family2)));

        // Override hashCode for family1 to introduce a discrepancy
        family2 = new Family(father2, mother2) {
            @Override
            public int hashCode() {
                return Objects.hash(getFather(), getMother());
            }
        };

        assertThat(family, is(not(equalTo(family2))));
        assertThat(family.hashCode(), is(not(equalTo(family2.hashCode()))));

        Man father4 = new Man("Charlie", "Brown", 1990);
        Woman mother4 = new Woman("Lucy", "Brown", 1992);
        Family family4 = new Family(father4, mother4);
        Human son_family3 = new Human("Bryan", "Brown", 2010);
        family4.addChild(son_family3);
        assertThat(family3, is(not(equalTo(family4))));
    }

    @Test
    public void testSetChildren() {
        Human child1 = new Human("Bob", "Doe", 1995);
        Human child2 = new Human("Alex", "Doe", 2000);

        Human[] newChildren = {child1, child2};

        family.setChildren(newChildren);

        assertThat(Arrays.asList(family.getChildren()), hasItems(child1, child2));
        assertThat(child1.getFamily(), is(sameInstance(family)));
        assertThat(child2.getFamily(), is(sameInstance(family)));
    }

    @Test
    public void testToString() {
        Human child1 = new Human("Bob", "Doe", 2005);
        Human child2 = new Human("Alice", "Doe", 2010);
        family.addChild(child1);
        family.addChild(child2);

        Pet myPet = new Dog(); // Assuming Dog is a subclass of Pet
        myPet.setNickname("Buddy");
        family.setPet(myPet);

        // Act
        String familyString = family.toString();

        // Assert with Hamcrest
        String expectedOutput = "Family{\n" +
                "  Father: " + family.getFather() + "\n" +
                "  Mother: " + family.getMother() + "\n" +
                "  Children: [" + child1 + ", " + child2 + "]\n" +
                "  Pet: " + myPet + "\n" +
                "  Total Persons in Family: 4\n" +
                "}";
        assertThat(familyString, Matchers.is(expectedOutput));
    }

    @Test
    public void testToStringWithNoChildrenAndNoPet() {
        String familyString = family.toString();

        String expectedOutput = "Family{\n" +
                "  Father: " + family.getFather() + "\n" +
                "  Mother: " + family.getMother() + "\n" +
                "  Children: No children in this family.\n" +
                "  Pet: null\n" +
                "  Total Persons in Family: 2\n" +
                "}";
        assertThat(familyString, is(equalTo(expectedOutput)));
    }
}
