package homework6Test;

import org.assertj.core.api.Assertions;
import org.homework6.*;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Objects;

import static org.assertj.core.api.Assertions.*;

public class FamilyTest_assertj {

    Family family = TestUtilities.setUpFamily();

    @Test
    public void addChildShouldAddChildToFamily() {
        Human child = new Human("Bob", "Doe", 1995);

        assertThat(family.addChild(child)).isTrue();
        assertThat(Arrays.asList(family.getChildren())).contains(child);
        assertThat(family.getChildren()).hasSize(1);
        assertThat(family.getChildren()[0]).isSameAs(child);
        assertThat(child.getFamily()).isSameAs(family);
    }

    @Test
    public void addChildShouldNotAddDuplicateChild() {
        Human child = new Human("Alex", "Doe", 2000);

        assertThat(family.addChild(child)).isTrue();
        assertThat(family.addChild(child)).isFalse();
    }

    @Test
    public void deleteChildShouldRemoveChildFromFamily() {
        Human child1 = new Human("Bob", "Doe", 1995);
        Human child2 = new Human("Alex", "Doe", 2000);

        assertThat(family.addChild(child1)).isTrue();
        assertThat(family.addChild(child2)).isTrue();

        family.deleteChild(child1);

        assertThat(Arrays.asList(family.getChildren())).doesNotContain(child1);

        Human nonExistentChild = new Human("Non", "Existent", 1998);
        int originalChildrenCount = family.getChildren().length;

        family.deleteChild(nonExistentChild);

        assertThat(family.getChildren()).hasSize(originalChildrenCount);
    }

    @Test
    public void deleteChildByIndexShouldRemoveChildFromFamily() {
        Human child1 = new Human("Bob", "Doe", 1995);
        Human child2 = new Human("Alex", "Doe", 2000);

        assertThat(family.addChild(child1)).isTrue();
        assertThat(family.addChild(child2)).isTrue();

        assertThat(family.deleteChild(0)).isTrue();
        assertThat(Arrays.asList(family.getChildren())).doesNotContain(child1);

        assertThat(family.deleteChild(2)).isFalse();
        assertThat(family.getChildren()).hasSize(1);
    }

    @Test
    public void testCountFamily() {
        assertThat(family.countFamily()).isEqualTo(2);

        Human child1 = new Human("Bob", "Doe", 1995);
        Human child2 = new Human("Alex", "Doe", 2000);

        assertThat(family.addChild(child1)).isTrue();
        assertThat(family.addChild(child2)).isTrue();

        assertThat(family.countFamily()).isEqualTo(4);
        family.deleteChild(child1);
        assertThat(family.countFamily()).isEqualTo(3);
    }

    @Test
    public void testSetFather_and_Mother() {
        Family family = new Family(null, null);
        Man father = new Man("John", "Doe", 1970);
        family.setFather(father);

        assertThat(family.getFather()).isEqualTo(father);
        assertThat(father.getFamily()).isEqualTo(family);

        Woman mother = new Woman("Jane", "Doe", 1975);
        family.setMother(mother);

        Man newFather = new Man("Bob", "Kennedy", 1970);
        family.setFather(newFather);

        assertThat(newFather.getFamily()).isNull();

        Woman newMother = new Woman("Marina", "Krotova", 1980);
        family.setMother(newMother);

        assertThat(newMother.getFamily()).isNull();
    }

    @Test
    public void testEquals_and_hashCode() {
        Man father2 = new Man("John", "Doe", 1970);
        Woman mother2 = new Woman("Jane", "Doe", 1975);
        Family family2 = new Family(father2, mother2);
        Man father3 = new Man("Charlie", "Brown", 1990);
        Woman mother3 = new Woman("Lucy", "Brown", 1992);
        Family family3 = new Family(father3, mother3);

        // Testing reflexivity
        assertThat(family).isEqualTo(family);

        // Testing consistency
        assertThat(family).isEqualTo(family2);

        // Testing symmetry
        assertThat(family2).isEqualTo(family);

        // Testing transitivity
        assertThat(family).isEqualTo(family2)
                .isEqualTo(family)
                .isEqualTo(family2);

        // Testing equality with null
        assertThat(family).isNotEqualTo(null);

        // Testing hash code consistency
        assertThat(family.hashCode()).isEqualTo(family2.hashCode());

        // Testing hash code inequality with different objects
        assertThat(family.hashCode()).isNotEqualTo(family3.hashCode());
        assertThat(family).isEqualTo(family2);

        // Override hashCode for family1 to introduce a discrepancy
        family2 = new Family(father2, mother2) {
            @Override
            public int hashCode() {
                return Objects.hash(getFather(), getMother());
            }
        };

        assertThat(family).isNotEqualTo(family2);
        assertThat(family.hashCode()).isNotEqualTo(family2.hashCode());

        Man father4 = new Man("Charlie", "Brown", 1990);
        Woman mother4 = new Woman("Lucy", "Brown", 1992);
        Family family4 = new Family(father4, mother4);
        Man son_family3 = new Man("Bryan", "Brown", 2010);
        family4.addChild(son_family3);
        assertThat(family3).isNotEqualTo(family4);
    }

    @Test
    public void testSetChildren() {
        Human child1 = new Human("Bob", "Doe", 1995);
        Human child2 = new Human("Alex", "Doe", 2000);

        Human[] newChildren = {child1, child2};

        family.setChildren(newChildren);

        assertThat(Arrays.asList(family.getChildren())).contains(child1, child2);
        assertThat(child1.getFamily()).isEqualTo(family);
        assertThat(child2.getFamily()).isEqualTo(family);
    }

    @Test
    public void testToString() {
        Human child1 = new Human("Bob", "Doe", 2005);
        Human child2 = new Human("Alice", "Doe", 2010);
        family.addChild(child1);
        family.addChild(child2);

        Dog myPet = new Dog(); // Assuming Dog is a subclass of Pet
        myPet.setNickname("Buddy");
        family.setPet(myPet);

        // Act
        String familyString = family.toString();

        // Assert
        Assertions.assertThat(familyString).isEqualToIgnoringWhitespace("Family{" +
                "  Father: " + family.getFather() +
                "  Mother: " + family.getMother() +
                "  Children: [" + child1 + ", " + child2 + "]" +
                "  Pet: " + myPet +
                "  Total Persons in Family: 4" +
                "}");
    }

    @Test
    public void testToStringWithNoChildrenAndNoPet() {
        String familyString = family.toString();

        String expectedOutput = "Family{\n" +
                "  Father: " + family.getFather() + "\n" +
                "  Mother: " + family.getMother() + "\n" +
                "  Children: No children in this family.\n" +
                "  Pet: null\n" +
                "  Total Persons in Family: 2\n" +
                "}";
        assertThat(familyString).isEqualTo(expectedOutput);
    }
}
