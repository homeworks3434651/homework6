package homework6Test;

import org.homework6.*;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.*;

import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Arrays;

public class HumanTest_hamcrest {

    Family family = TestUtilities.setUpFamily();

    @Test
    public void testSetFamily() {
        Human human3 = new Human("Alex", "Doe", 2000);

        assertThat(human3.setFamily(family), is(true));
        assertThat(human3.getFamily(), is(equalTo(family)));

        Family family2 = new Family(new Man("Bob", "Smith", 1990), new Woman("Alice", "Smith", 1995));

        assertThat(human3.setFamily(family2), is(false));
        assertThat(human3.getFamily(), is(equalTo(family))); // Family should remain unchanged
    }

    @Test
    public void testFeedPetWhenTimeToFeed() {
        Dog pet = new Dog(); // Assuming Dog is a subclass of Pet
        pet.setNickname("Buddy");
        pet.setTrickLevel(52);
        family.setPet(pet);

        // Test feeding when it's time to feed
        assertThat(family.getFather().feedPet(true), is(true));
    }

    @Test
    public void testFeedPet_shouldReturnTrueOrFalse() {
        DomesticCat cat = new DomesticCat("Whiskers");
        // Assuming setTrickLevel generates a random value between 0 and 100
        cat.setTrickLevel(50);

        family.setPet(cat);

        // Now, instead of expecting a specific result, you can assert that it's either true or false
        assertThat(family.getFather().feedPet(false), anyOf(is(true), is(false)));
    }

    @Test
    public void testFeedPet() {
        DomesticCat cat = new DomesticCat("Whiskers");
        family.setPet(cat);

        // Test when it's not time to feed, and pet trick level is higher
        family.getPet().setTrickLevel(100);
        assertThat(family.getFather().feedPet(false), is(true));

        // Test when it's not time to feed, and pet trick level is small
        family.getPet().setTrickLevel(0);
        assertThat(family.getFather().feedPet(false), is(false));
    }

    @Test
    public void testGreetPet() {
        Human child = new Human("Alex", "Doe", 2000);
        family.addChild(child);

        DomesticCat cat = new DomesticCat("Whiskers");
        family.setPet(cat);

        assertThat(family.getMother().greetPet(), is("Hello, I greet you Whiskers"));

        assertThat(family.getFather().greetPet(), is("Hello, my favorite friend Whiskers"));

        assertThat(child.greetPet(), is("Hello, Whiskers"));

        // Test greeting when there is no pet
        family.setPet(null);
        assertThat(family.getMother().greetPet(), is("I don't have a pet."));
    }

    @Test
    public void testDescribePet() {
        Dog pet = new Dog(); // Assuming Dog is a subclass of Pet
        pet.setNickname("Buddy");
        pet.setAge(3);
        pet.setTrickLevel(52);
        family.setPet(pet);

        // Test describing the pet
        assertThat(family.getMother().describePet(), is(equalTo("I have a DOG. It is 3 years old, and it is very cunning.")));

        // Test describing when there is no pet
        family.setPet(null);
        assertThat(family.getMother().describePet(), is(equalTo("I don't have a pet.")));
    }

    @Test
    public void testRepairCar() {
        Man man = new Man("John", "Doe", 1980);

        // Redirect System.out to capture the printed output
        ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStreamCaptor));

        // Call the repairCar() method
        man.repairCar();

        // Reset the standard output stream
        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));

        // Check only the output related to the repairCar() method
        String expectedOutput = "I'm repairing the car.";
        String actualOutput = outputStreamCaptor.toString().trim();  // Remove leading/trailing whitespaces

        assertThat(actualOutput, equalTo(expectedOutput));
    }

    @Test
    public void testMakeUp() {
        Woman woman = new Woman("Alice", "Smith", 1980);

        // Redirect System.out to capture the printed output
        ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStreamCaptor));

        // Call the makeup() method
        woman.makeup();

        // Reset the standard output stream
        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));

        // Check only the output related to the makeup() method
        String expectedOutput = "I'm applying makeup.";
        String actualOutput = outputStreamCaptor.toString().trim();  // Remove leading/trailing whitespaces

        assertThat(actualOutput, equalTo(expectedOutput));
    }

    @Test
    public void testBornChildWithNullName() {
        family.getMother().setIQ(105);
        family.getFather().setIQ(125);

        Human child = family.getMother().bornChild(null, "Doe", family, 115);

        assertThat(child, is(notNullValue()));
        assertThat(child.getName(), is(not(emptyString())));
        assertThat(child.getSurname(), is(equalTo("Doe")));
        assertThat(child.getFamily(), is(equalTo(family)));

        int fatherIQ = family.getFather().getIQ();
        int motherIQ = family.getMother().getIQ();
        int inheritedIQ = family.getMother().calculateInheritedIQ(fatherIQ, motherIQ);

        assertThat(child.getIQ(), is(equalTo(inheritedIQ)));
        assertThat(child, anyOf(instanceOf(Man.class), instanceOf(Woman.class)));

        String randomMaleName = family.getMother().getRandomMaleName();
        assertThat(randomMaleName, is(notNullValue()));
        assertThat(Arrays.asList(family.getMother().MALE_NAMES), hasItem(randomMaleName));

        String randomFemaleName = family.getMother().getRandomFemaleName();
        assertThat(randomFemaleName, is(notNullValue()));
        assertThat(Arrays.asList(family.getMother().FEMALE_NAMES), hasItem(randomFemaleName));

        assertThat(family.getMother().isMaleName("John"), is(true));
        assertThat(family.getMother().isMaleName("Alice"), is(false));
    }

    @Test
    public void testEquals_and_hashCode() {
        Human human1 = new Human("John", "Doe", 1980);
        Human human2 = new Human("John", "Doe", 1980);
        Human human3 = new Human("Jane", "Doe", 1985);

        assertThat(human1, is(equalTo(human2)));
        assertThat(human1, is(not(equalTo(human3))));

        assertThat(human1.hashCode(), is(equalTo(human2.hashCode())));
        assertThat(human1.hashCode(), is(not(equalTo(human3.hashCode()))));
    }

    @Test
    public void testToString() {
        Family family = new Family(new Man("Charlie", "Brown", 1990),
                new Woman("Lucy", "Brown", 1992));

        Human human = new Human("Bryan", "Brown", 2010, 120, family,
                DayOfWeek.TUESDAY.name(), "Studying", DayOfWeek.SATURDAY.name(), "Gym");

        assertThat(human.toString(),
                is("Human{name='Bryan', surname='Brown', year=2010, iq=120, schedule=[[TUESDAY, Studying], [SATURDAY, Gym]]}"));

        Man man = new Man("John", "Doe", 1980);
        man.setIQ(120);

        // Test the string representation of the man
        assertThat(man.toString(),
                is("Man{name='John', surname='Doe', year=1980, iq=120, schedule=[]}"));

        Woman woman = new Woman("Alice", "Smith", 1980);

        // Test the string representation of the woman
        assertThat(woman.toString(),
                is("Woman{name='Alice', surname='Smith', year=1980, iq=0, schedule=[]}"));
    }
}
