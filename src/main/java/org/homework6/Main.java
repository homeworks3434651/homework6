package org.homework6;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        // Family 1
        Woman woman_family1 = new Woman("Jane", "Doe", 1975);
        woman_family1.setIQ(124);

        Family family1 = new Family(new Man("John", "Doe", 1970), woman_family1);
        family1.getFather().setIQ(120);

        Dog pet_family1 = new Dog();
        pet_family1.setNickname("Buddy");
        pet_family1.setAge(4);
        pet_family1.setTrickLevel(55);
        String[] pet_family1_habits = {"eat", "drink", "sleep"};
        pet_family1.setHabits(pet_family1_habits);

        family1.setPet(pet_family1);
        System.out.println(family1.getFather());

        System.out.println(family1.getFather().greetPet());
        System.out.println(woman_family1.greetPet());
        family1.getFather().repairCar();
        woman_family1.makeup();

        // Use getters
        System.out.println("\nPet in Family 1:");
        System.out.println(family1.getPet());
        System.out.println("\nHabits of pet_for_child_family1:");
        System.out.println(Arrays.toString(pet_family1.getHabits()));

        pet_family1.eat();
        pet_family1.respond();
        pet_family1.foul();

        System.out.println(family1.getMother());
        // Calling the bornChild method to create a new child
        Human newChildFamily1 = family1.getMother().bornChild(null, "Doe", family1, 110);

        // Displaying information about the newly created child
        System.out.println("\nNew child in Family 1:");
        System.out.println(newChildFamily1);

        Human child_family1 = new Human("Alex", "Doe", 2000, 120, family1,
               new String[][] {{DayOfWeek.FRIDAY.name(), "Chess"}, {DayOfWeek.MONDAY.name(), "Soccer"}});

        System.out.println("Activity for FRIDAY: " + DayOfWeek.getActivityForDay(DayOfWeek.FRIDAY));
        System.out.println("Activity for SATURDAY: " + DayOfWeek.getActivityForDay(DayOfWeek.SATURDAY));

        String activityForAlexMonday = DayOfWeek.getActivityForDay(DayOfWeek.MONDAY, child_family1.getSchedule());
        System.out.println("Activity for Alex MONDAY: " + activityForAlexMonday);
        String activityForAlexSaturday = DayOfWeek.getActivityForDay(DayOfWeek.SATURDAY, child_family1.getSchedule());
        System.out.println("Activity for Alex SATURDAY: " + activityForAlexSaturday);

        System.out.println(child_family1.getSchedule());

        System.out.println("\nChild in Family 1:");
        System.out.println(child_family1);

        child_family1.greetPet();
        child_family1.describePet();
        System.out.println();

        family1.getFather().feedPet(false);

        family1.addChild(child_family1);
        family1.addChild(new Human("Bob", "Doe", 1995));
        family1.addChild(new Human("Davida", "Doe", 2000, family1));

        System.out.println("\nFamily 1 (with pets and children):");
        System.out.println(family1);
        System.out.println();

        family1.deleteChild(family1.getChildren()[0]);
        family1.deleteChild(new Human("Bob", "Doe", 1995));   // I get the error message
        family1.deleteChild(new Human("Bob", "Doe", 1995));   // I get the error message
        family1.deleteChild(1);
        family1.deleteChild(family1.getChildren()[0]);
        try {
            family1.deleteChild(family1.getChildren()[0]);
        } catch (IndexOutOfBoundsException e) {
            System.out.println(e.getMessage());    // I get the exception message
        }
        family1.deleteChild(0);  // I get the error message
        try {
            family1.deleteChild(family1.getChild(10));
        } catch (IndexOutOfBoundsException e) {
            System.out.println(e.getMessage());    // I get the exception message
        }

        Human[] newChildren_family1 = {
                new Human("Emily", "Doe", 2012),
                new Human("Brandon", "Doe", 2014, family1),
                new Human("Landon", "Doe", 2017)
        };

        family1.setChildren(newChildren_family1);

        // Create a second Pet for family1
        RoboCat pet_family1_second = new RoboCat("Max");
        Species.ROBO_CAT.setCanFly(true);
        Species.ROBO_CAT.setNumberOfLegs(2);
        Species.ROBO_CAT.setHasFur(true);
        pet_family1_second.setAge(3);
        pet_family1_second.setTrickLevel(40);
        pet_family1_second.setHabits(new String[]{"play", "fetch"});
        family1.setPet(pet_family1_second);

        // Use getters
        System.out.println("\nAnother Pet in Family 1:");
        System.out.println(family1.getPet());
        System.out.println();

        family1.getChildren()[1].feedPet(true);

        Human lastChild = family1.getChildren()[family1.getChildren().length - 1];
        System.out.println("\nLast Child in Family 1:");
        System.out.println(lastChild);
        lastChild.greetPet();
        lastChild.describePet();

        System.out.println("\nFamily 1 after setting new children and the new pet:");
        System.out.println(family1);

        // Set a new father
        Human newFather_family1 = new Human("Bob", "Kennedy", 1970);
        family1.setFather(newFather_family1);  // I get the error message

        // Family 2
        Man father_family2 = new Man("Bob", "Smith", 1975);
        Woman mother_family2 = new Woman("Alice", "Smith", 1980);
        Family family2 = new Family(father_family2, mother_family2);

        DomesticCat pet_family2 = new DomesticCat( "Whiskers");
        family2.setPet(pet_family2);

        Human child_family2 = new Human("Ryan", "Smith", 2005, family2);
        family2.addChild(child_family2);
        System.out.println("\nChild in Family 2:");
        System.out.println(child_family2);

        child_family2.setFamily(family1);  // I get the error message

        family2.addChild(new Human("Debora", "Smith", 2010));
        family2.addChild(new Human("Ruth", "Smith", 2015));

        pet_family2.setAge(2);
        pet_family2.setTrickLevel(60);
        System.out.println("\nPet in Family 2:");
        System.out.println(pet_family2);
        System.out.println();

        family2.getChildren()[2].feedPet(false);

        System.out.println("\nFamily 2 (with pets and children):");
        System.out.println(family2);

        // Set a new mother
        Human newMother = new Human("Marina", "Krotova", 1980, family2);
        family2.setMother(newMother);   // I get the error message
        System.out.println();

        // Family 3
        Family family3 = new Family(new Man("Charlie", "Brown", 1990),
                                    new Woman("Lucy", "Brown", 1992));

        Human son_family3 = new Human("Bryan", "Brown", 2010, 120, family3,
                DayOfWeek.SATURDAY.name(), "Gym", DayOfWeek.TUESDAY.name(), "Studying");

        family3.addChild(son_family3);
        son_family3.addDayToSchedule(DayOfWeek.FRIDAY.name(), "Hiking");

        System.out.println("Son in Family 3:");
        System.out.println(son_family3);

        // Use getters
        Human sonFamily3 = family3.getChildren()[0];

        System.out.println("\nSon in Family 3 - IQ: " + sonFamily3.getIQ());
        System.out.println("Son in Family 3 - Schedule: " + sonFamily3.getSchedule());
        String activityForTuesday = DayOfWeek.getActivityForDay(DayOfWeek.TUESDAY, sonFamily3.getSchedule());
        System.out.println("Activity for TUESDAY: " + activityForTuesday);
        String activityForWednesday = DayOfWeek.getActivityForDay(DayOfWeek.WEDNESDAY, sonFamily3.getSchedule());
        System.out.println("Activity for WEDNESDAY: " + activityForWednesday);

        Fish pet_family3 = new Fish("Polly", 3, 70, new String[]{"swimming", "exploring", "resting"});
        family3.setPet(pet_family3);

        System.out.println("Son in Family 3 - Pet: " + family3.getPet());
        pet_family3.eat();
        pet_family3.respond();
        family3.getMother().feedPet(false);

        Human daughter_family3 = new Human();

        daughter_family3.setName("Laura");
        daughter_family3.setSurname("Brown");
        daughter_family3.setYear(2006);
        daughter_family3.setIQ(100);
        // daughter_family3.setFamily(family3);
        String[][] scheduleForLaura = {
                {DayOfWeek.WEDNESDAY.name(), "Run"},
                {DayOfWeek.SUNDAY.name(), "Dance Class"},
                {DayOfWeek.MONDAY.name(), "Сooking"},
        };
        DayOfWeek.THURSDAY.setActivity("Party");

        String activityForLauraMonday = DayOfWeek.getActivityForDay(DayOfWeek.MONDAY, scheduleForLaura);
        System.out.println("Activity for Alex MONDAY: " + DayOfWeek.MONDAY.getActivity());
        System.out.println("Activity for Bryan FRIDAY: " + DayOfWeek.FRIDAY.getActivity());

        System.out.println("Activity for Laura MONDAY: " + activityForLauraMonday);
        System.out.println("Activity for THURSDAY: " + DayOfWeek.THURSDAY.getActivity());

        daughter_family3.setSchedule(scheduleForLaura);
        daughter_family3.addDayToSchedule(DayOfWeek.THURSDAY.name(), DayOfWeek.THURSDAY.getActivity());
        System.out.println(daughter_family3.getSchedule());
        System.out.println("Activity for Laura MONDAY: " + DayOfWeek.MONDAY.getActivity());

        family3.addChild(daughter_family3);
        System.out.println("\nDaughter in Family 3:");
        System.out.println(daughter_family3);

        System.out.println("\nFamily 3 (with pets and children):");
        System.out.println(family3);
        System.out.println();

        // False families
        Family false_family1 = new Family(father_family2, new Woman("Marina", "Krotova", 1980));
        System.out.println(false_family1);  // I get the error message

        Family false_family2 = new Family(new Man("Charlie", "Brown", 1990), mother_family2);
        System.out.println(false_family2);  // I get the error message

        Family false_family3 = new Family(new Man("Mike", "Jackson", 1985),
                                          new Woman("Mike", "Jackson", 1985));
        System.out.println(false_family3);  // I get the error message

        // Invalid family and fixing it
        Family valid_family = new Family(null, null);
        System.out.println(valid_family.getFather());
        System.out.println(valid_family.getMother());

        // Set a mother
        Woman mother_valid_family = new Woman("Irin", "Peterson", 1981);
        valid_family.setMother(mother_valid_family);
        System.out.println(valid_family.getMother());

        // Set a father
        Man father_valid_family = new Man("Ted", "Jansen", 1976);
        valid_family.setFather(father_valid_family);
        System.out.println(valid_family.getFather());
        System.out.println(valid_family);

        // Create an unknown Pet
        Pet pet_valid_family = new Pet("Petryk", 3, 65, new String[]{"play", "fetch"}) {
            @Override
            public void respond() {

            }

            @Override
            public void eat() {

            }
        };

        valid_family.setPet(pet_valid_family);

        father_valid_family.feedPet(false);

        System.out.println("\nFamily after setting the new pet:");
        System.out.println(valid_family);
    }
}
