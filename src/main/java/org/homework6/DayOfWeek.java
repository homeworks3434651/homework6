package org.homework6;

public enum DayOfWeek {
    MONDAY(""),
    TUESDAY(""),
    WEDNESDAY(""),
    THURSDAY(""),
    FRIDAY(""),
    SATURDAY(""),
    SUNDAY("");

    private String activity;

    DayOfWeek(String activity) {
        this.activity = activity;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public static String getActivityForDay(DayOfWeek day) {
        String activity = day.getActivity();
        return (activity != null && !activity.isEmpty()) ? activity : "No activity specified";
    }

    public static String getActivityForDay(DayOfWeek day, String[][] schedule) {
        for (String[] dayActivity : schedule) {
            if (dayActivity[0].equals(day.name())) {
                return dayActivity[1];
            }
        }
        return "No activity specified";
    }

    public static String getActivityForDay(DayOfWeek day, String... schedule) {
        for (String item : schedule) {
            item = item.replace("[", "").replace("]", ""); // Remove square brackets if present
            String[] items = item.split(", ");

            for (int i = 0; i < items.length - 1; i += 2) {
                String currentDay = items[i];
                String currentActivity = items[i + 1];

                if (DayOfWeek.valueOf(currentDay) == day) {
                    return currentActivity;
                }
            }
        }
        return "No activity specified";
    }
}
