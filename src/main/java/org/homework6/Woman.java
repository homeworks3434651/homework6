package org.homework6;

import java.util.Arrays;
import java.util.Random;

public final class Woman extends Human implements HumanCreator{

    public Woman() {
        super();
    }

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, Family family) {
        super(name, surname, year, family);
    }

    public Woman(String name, String surname, Family family, int iq) {
        super(name, surname);
        this.setFamily(family);
        this.setIQ(iq);
    }

    public Woman(String name, String surname, int year, int iq, Family family, String... schedule) {
        super(name, surname, year, iq, family, schedule);
    }

    @Override
    public String greetPet() {
        String result;
        if (this.getFamily() != null && this.getFamily().getPet() != null) {
            result = "Hello, I greet you " + this.getFamily().getPet().getNickname();
        } else {
            result = "I don't have a pet.";
        }
        return result;
    }

    public void makeup() {
        System.out.println("I'm applying makeup.");
    }

    public static final String[] CHILD_NAMES = {"John", "Alice", "Bob", "Eva", "Charlie", "Olivia"};
    public static final String[] MALE_NAMES = {"John", "Bob", "Charlie"};
    public static final String[] FEMALE_NAMES = {"Alice", "Eva", "Olivia"};

    @Override
    public Human bornChild(String childName, String fatherSurname, Family family, int averageIQ) {
        if (childName == null || childName.isEmpty()) {
            Random random = new Random();
            childName = CHILD_NAMES[random.nextInt(CHILD_NAMES.length)];
        }

        // Determine the gender randomly (50% chance for Man or Woman)
        boolean isMale = new Random().nextBoolean();

        // Create a new instance of Man or Woman based on the determined gender

        Human child;

        if (isMale) {
            child = new Man(childName, fatherSurname, family, averageIQ);
        } else {
            child = new Woman(childName, fatherSurname, family, averageIQ);
        }

        family.addChild(child);

        // Check if the name matches the gender and replace it if needed
        if (isMale && !isMaleName(childName)) {
            childName = getRandomMaleName();
            child.setName(childName);
        } else if (!isMale && isMaleName(childName)) {
            childName = getRandomFemaleName();
            child.setName(childName);
        }

        int inheritedIQ = calculateInheritedIQ(family.getFather().getIQ(), family.getMother().getIQ());
        child.setIQ(inheritedIQ);

        return child;
    }

    public int calculateInheritedIQ(int fatherIQ, int motherIQ) {
        return (fatherIQ + motherIQ) / 2;
    }

    // Additional methods for getting random names of specific gender
    public String getRandomMaleName() {
        Random random = new Random();
        return MALE_NAMES[random.nextInt(MALE_NAMES.length)];
    }

    public String getRandomFemaleName() {
        Random random = new Random();
        return FEMALE_NAMES[random.nextInt(FEMALE_NAMES.length)];
    }

    public boolean isMaleName(String name) {
        for (String maleName : MALE_NAMES) {
            if (maleName.equals(name)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "Woman{" +
                "name='" + getName() + '\'' +
                ", surname='" + getSurname() + '\'' +
                ", year=" + getYear() +
                ", iq=" + getIQ() +
                ", schedule=" + getSchedule() +
                '}';
    }
}
