package org.homework6;

public class DomesticCat extends Pet implements Foulable {

    // Empty constructor
    public DomesticCat() {
        super();
        setSpecies(Species.DOMESTIC_CAT);
    }

    public DomesticCat(String nickname) {
        super(nickname);
        setSpecies(Species.DOMESTIC_CAT);
    }

    public DomesticCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies(Species.DOMESTIC_CAT);
    }

    @Override
    public void respond() {
        System.out.println("Meow! I'm a domestic cat.");
    }

    @Override
    public void eat() {
        System.out.println("I eat cat food.");
    }

    @Override
    public void foul() {
        System.out.println("I just knocked over your favorite plant.");
    }
}
