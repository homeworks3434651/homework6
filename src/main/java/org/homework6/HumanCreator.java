package org.homework6;

public interface HumanCreator {

    Human bornChild(String childName, String fatherSurname, Family family, int averageIQ);
}
