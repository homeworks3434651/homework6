package org.homework6;

import java.util.Arrays;

public final class Man extends Human {

    public Man() {
        super();
    }

    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, Family family) {
        super(name, surname, year, family);
    }

    public Man(String name, String surname, Family family, int iq) {
        super(name, surname);
        this.setFamily(family);
        this.setIQ(iq);
    }

    public Man(String name, String surname, int year, int iq, Family family, String... schedule) {
        super(name, surname, year, iq, family, schedule);
    }

    @Override
    public String greetPet() {
        String result;
        if (this.getFamily() != null && this.getFamily().getPet() != null) {
            result = "Hello, my favorite friend " + this.getFamily().getPet().getNickname();
        } else {
            result = "I don't have a pet.";
        }
        return result;
    }

    public void repairCar() {
        System.out.println("I'm repairing the car.");
    }

    @Override
    public String toString() {
        return "Man{" +
                "name='" + getName() + '\'' +
                ", surname='" + getSurname() + '\'' +
                ", year=" + getYear() +
                ", iq=" + getIQ() +
                ", schedule=" + getSchedule() +
                '}';
    }
}
