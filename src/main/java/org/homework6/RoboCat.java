package org.homework6;

public class RoboCat extends Pet {

    // Empty constructor
    public RoboCat() {
        super();
        setSpecies(Species.ROBO_CAT);
    }

    public RoboCat(String nickname) {
        super(nickname);
        setSpecies(Species.ROBO_CAT);
    }

    public RoboCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies(Species.ROBO_CAT);
    }

    @Override
    public void respond() {
        System.out.println("Beep boop! I'm a robotic cat.");
    }

    @Override
    public void eat() {
        System.out.println("I consume electricity to recharge.");
    }
}
