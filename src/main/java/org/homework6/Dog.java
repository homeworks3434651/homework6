package org.homework6;

public class Dog extends Pet implements Foulable {

    // Empty constructor
    public Dog() {
        super();
        setSpecies(Species.DOG);
    }

    public Dog(String nickname) {
        super(nickname);
        setSpecies(Species.DOG);
    }

    public Dog(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies(Species.DOG);
    }

    @Override
    public void respond() {
        System.out.println("Woof! I'm a dog.");
    }

    @Override
    public void eat() {
        System.out.println("I eat meat.");
    }

    @Override
    public void foul() {
        System.out.println("I left a little surprise for you on the carpet.");
    }
}
